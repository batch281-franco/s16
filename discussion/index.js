// javascript oprators
// arethmetic operators


let x = 1397;
	y = 7831;

let sum = x + y;
console.log("result of addition operator: " + sum);

let difference = x - y;
console.log("result of subraction operator " + difference);

let product = x * y;
console.log("result of multiplication " + product);

let quotation = x / y;
console.log("result of division operator: " + quotation);

let remainder = y % x;
console.log("result of modulo operator: " + remainder);

// assignment operators (=) - assign the value of the right operand to a variable

let assignmentNumber = 8;

// adition assignment operator (+=)

assignmentNumber = assignmentNumber + 2;
console.log("result of addition assignment operator: "+ assignmentNumber);

// shorthand
assignmentNumber += 2;
console.log("result of addition assignment operator: " + assignmentNumber);

// subtraction/multiplication/division assignment oprator (-=, *=, /=)

assignmentNumber -= 2;
console.log("result of subraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("result of multiplication assignment operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("result of division assignment operator: " + assignmentNumber);

// multiple operators and parentheses

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("result of mdas operator: " + mdas);

// using paretheses
let pemdas = 1 + (2 - 3) * (4 / 5); 
console.log("result of mdas operator: " + pemdas);

pemdas = (1 + (2 - 3)) * (4 / 5); 
console.log("result of pemdas operator: " + pemdas);

// increment and decrement (++/--)
let z = 1;
//  pre-increment
let increment = ++z;
console.log("result of pre-increment: " + increment);
console.log("result of pre-increment: " + z);

// post-increment
increment = z++;
console.log("result of post-increment: " + increment);
console.log("result of post-increment: " + z);

//

let decrement = --z;
console.log("result of pre-decrement: " + decrement);
console.log("result of pre-decrement: " + z);

decrement = z--;
console.log("result of post-decrement: " + decrement);
console.log("result of post-decrement: " + z);

// type coercion 

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numc = true + 1;
console.log(numc);

// comparison operators 

let juan = 'juan';

// equality operator (==)  
 console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log('juan' == juan);

// inequality operator (!=);
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' != juan);

// strict equality operator (***)

console.log(1 ** 1);
console.log(1 ** 2);
console.log(1 ** '1');
console.log(0 ** false);
console.log('juan' ** juan);


// strict inequality operator (!==)
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== juan);

// relational operators 
let a = 50
let b = 65;

//  greater than (>)
let isgreaterthan = a > b;

let islessthan = a < b;

let isgtorequal = a >= b;

let isltorequal = a <= b;

console.log(isgreaterthan);
console.log(islessthan);
console.log(isgtorequal);
console.log(isltorequal);


// logical operators
let islegalage = true;
let isregistered = false;

// logical and operator (&&)
// return true if all operand are true
let allrequirementsmet = islegalage && isregistered;
console.log("result of logical and operator: all " + requirementsmet)

// logical or operator(||)
// return true if one of the operands are true

let somerequirementsmet = islegalage || isregistered;
console.log("result of logical or operators: " + somerequirementsmet)


// logical not operator (!)
//  return the opposite value

let somerequirementsnotmet = !isregistered;
console.log(somerequirementsnotmet)



